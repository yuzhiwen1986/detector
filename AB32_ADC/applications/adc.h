/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-11-21     jacyu       the first version
 */
#include <rtthread.h>
#include "board.h"
static int adc_vol_sample(int argc, char *argv[]);
