################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../rt-thread/components/finsh/cmd.c \
../rt-thread/components/finsh/msh.c \
../rt-thread/components/finsh/shell.c 

OBJS += \
./rt-thread/components/finsh/cmd.o \
./rt-thread/components/finsh/msh.o \
./rt-thread/components/finsh/shell.o 

C_DEPS += \
./rt-thread/components/finsh/cmd.d \
./rt-thread/components/finsh/msh.d \
./rt-thread/components/finsh/shell.d 


# Each subdirectory must supply rules for building sources it contributes
rt-thread/components/finsh/%.o: ../rt-thread/components/finsh/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"C:\RT-ThreadStudio\workspace\AB32_ADC" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\applications" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\board" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libcpu\cpu" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libraries\hal_drivers\config" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libraries\hal_drivers" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libraries\hal_libraries\ab32vg1_hal\include" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libraries\hal_libraries\ab32vg1_hal" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libraries\hal_libraries\bmsis\include" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libraries\hal_libraries\bmsis" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\packages\bluetrum_sdk-latest" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\rt-thread\components\drivers\include" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\rt-thread\components\drivers\spi" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\rt-thread\components\finsh" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\rt-thread\components\libc\compilers\common" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\rt-thread\components\libc\compilers\gcc\newlib" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\rt-thread\include" -isystem"C:\RT-ThreadStudio\workspace\AB32_ADC" -include"C:\RT-ThreadStudio\workspace\AB32_ADC\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

