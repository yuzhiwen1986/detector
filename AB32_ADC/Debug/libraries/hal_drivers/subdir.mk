################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../libraries/hal_drivers/drv_adc.c \
../libraries/hal_drivers/drv_common.c \
../libraries/hal_drivers/drv_gpio.c \
../libraries/hal_drivers/drv_usart.c 

OBJS += \
./libraries/hal_drivers/drv_adc.o \
./libraries/hal_drivers/drv_common.o \
./libraries/hal_drivers/drv_gpio.o \
./libraries/hal_drivers/drv_usart.o 

C_DEPS += \
./libraries/hal_drivers/drv_adc.d \
./libraries/hal_drivers/drv_common.d \
./libraries/hal_drivers/drv_gpio.d \
./libraries/hal_drivers/drv_usart.d 


# Each subdirectory must supply rules for building sources it contributes
libraries/hal_drivers/%.o: ../libraries/hal_drivers/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"C:\RT-ThreadStudio\workspace\AB32_ADC" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\applications" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\board" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libcpu\cpu" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libraries\hal_drivers\config" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libraries\hal_drivers" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libraries\hal_libraries\ab32vg1_hal\include" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libraries\hal_libraries\ab32vg1_hal" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libraries\hal_libraries\bmsis\include" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\libraries\hal_libraries\bmsis" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\packages\bluetrum_sdk-latest" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\rt-thread\components\drivers\include" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\rt-thread\components\drivers\spi" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\rt-thread\components\finsh" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\rt-thread\components\libc\compilers\common" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\rt-thread\components\libc\compilers\gcc\newlib" -I"C:\RT-ThreadStudio\workspace\AB32_ADC\rt-thread\include" -isystem"C:\RT-ThreadStudio\workspace\AB32_ADC" -include"C:\RT-ThreadStudio\workspace\AB32_ADC\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

